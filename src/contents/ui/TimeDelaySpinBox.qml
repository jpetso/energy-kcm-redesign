/*
    SPDX-FileCopyrightText: 2018 Tomaz Canabrava <tcanabrava@kde.org>

    SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
*/

import QtQuick 2.9
import QtQuick.Controls 2.14 as QQC2

QQC2.SpinBox {
    property string type: "minute"

    textFromValue: function(value, locale) {
        return (value < 10 ? "  " : "") + i18np("after %1 min", "after %1 min", value);
    }

    valueFromText: function(value, locale) {
        // Don't use fromLocaleString here since it will error out on extra
        // characters like the (potentially translated) seconds that gets
        // added above. Instead parseInt ignores non-numeric characters.
        let v = parseInt(value)
        console.log(value, "parseado ficou", v);
        if (isNaN(v)) {
            return 0
        }
        console.log("Retornando", v)
        return v
    }
}
