import QtQml 2.15
import QtQuick 2.15
import QtQuick.Controls 2.15 as QQC2
import QtQuick.Layouts 1.2 as Layouts
import org.kde.kirigami 2.14 as Kirigami

// CollapsedSettingsField and ExpandedSettingsField are split because FormLayout label alignment
// doesn't suffice for this use case - regular alignment would make it go up and down as a
// settings field group widget expands and contracts; TopAlign puts the label too far up,
// not aligned with other text. By keeping CollapsedSettingsField for just the top row,
// we prevent the worst in terms of label alignment. It does make for a weird structure though.
Kirigami.ScrollablePage {
    id: root

    globalToolBarStyle: Kirigami.ApplicationHeaderStyle.None

    Kirigami.FormLayout {
        QQC2.ComboBox {
            id: energyProfileShown
            Kirigami.FormData.label: "Settings for"

            textRole: "text"
            valueRole: "value"
            model: [
                { text: "On AC power", value: "power.ac" },
                { text: "On battery", value: "power.battery" },
                { text: "On low battery", value: "power.battery.low" },
                { text: "---", value: null },
                { text: "Default activity", value: "activity.a" },
                { text: "My custom activity", value: "activity.b" },
            ]
        }

        Kirigami.Separator {
            Kirigami.FormData.isSection: true
        }
        // Brightness

        CollapsedSettingsField {
            Kirigami.FormData.label: "Screen brightness:"
            expandedBuddy: expandedScreenBrightness

            Layouts.Layout.fillWidth: true
        }
        ExpandedSettingsField {
            id: expandedScreenBrightness
            currentEnergyProfile: energyProfileShown.currentValue

            Layouts.Layout.fillWidth: true

            onAcPowerWidget: QQC2.Slider {
                enabled: true
                from: 0
                to: 100
                value: 90
            }
            onBatteryWidget: QQC2.Slider {
                enabled: true
                from: 0
                to: 100
                value: 50
            }
            onLowBatteryWidget: QQC2.Slider {
                enabled: false
                from: 0
                to: 100
                value: 30
            }
            fallbackWidget: QQC2.Slider {
                from: 0
                to: 100
                value: 90
            }
        }

        CollapsedSettingsField {
            Kirigami.FormData.label: "Keyboard brightness:"
            expandedBuddy: expandedKeyboardBrightness

            Layouts.Layout.fillWidth: true
        }
        ExpandedSettingsField {
            id: expandedKeyboardBrightness
            currentEnergyProfile: energyProfileShown.currentValue

            Layouts.Layout.fillWidth: true

            onAcPowerWidget: QQC2.Slider {
                enabled: false
                from: 0
                to: 100
                value: 0
            }
            onBatteryWidget: QQC2.Slider {
                enabled: false
                from: 0
                to: 100
                value: 0
            }
            onLowBatteryWidget: QQC2.Slider {
                enabled: false
                from: 0
                to: 0
                value: 0
            }
            fallbackWidget: QQC2.Slider {
                from: 0
                to: 100
                value: 0
            }
        }

        CollapsedSettingsField {
            Kirigami.FormData.label: "Dim automatically:"
            expandedBuddy: expandedScreenDimming
        }
        ExpandedSettingsField {
            id: expandedScreenDimming
            currentEnergyProfile: energyProfileShown.currentValue

            onAcPowerWidget: TimeDelaySpinBox {
                type: "minute"
                value: 10
            }
            onBatteryWidget: TimeDelaySpinBox {
                type: "minute"
                value: 5
            }
            onLowBatteryWidget: TimeDelaySpinBox {
                type: "minute"
                value: 2
            }
            fallbackWidget: TimeDelaySpinBox {
                type: "minute"
                value: 10
            }
        }
        CollapsedSettingsField {
            Kirigami.FormData.label: "Turn off screen:"
            expandedBuddy: expandedScreenOff
        }
        ExpandedSettingsField {
            id: expandedScreenOff
            currentEnergyProfile: energyProfileShown.currentValue

            onAcPowerWidget: TimeDelaySpinBox {
                enabled: false
                type: "minute"
                value: 20
            }
            onBatteryWidget: TimeDelaySpinBox {
                enabled: false
                type: "minute"
                value: 10
            }
            onLowBatteryWidget: TimeDelaySpinBox {
                enabled: false
                type: "minute"
                value: 4
            }
            fallbackWidget: TimeDelaySpinBox {
                type: "minute"
                value: 20
            }
        }

        Kirigami.Separator {
            Kirigami.FormData.isSection: true
        }
        // Suspend session

        //
        // Suspend actions

        CollapsedSettingsField {
            Kirigami.FormData.label: "Suspend automatically:"
            expandedBuddy: expandedSuspendAutomatically
        }
        ExpandedSettingsField {
            id: expandedSuspendAutomatically
            currentEnergyProfile: energyProfileShown.currentValue

            onAcPowerWidget: Layouts.RowLayout {
                QQC2.ComboBox {
                    id: onAcPowerSuspendAutomaticallySelector
                    currentIndex: 0
                    model: ListModel {
                        ListElement { text: "No action" }
                        ListElement { text: "Sleep" }
                        ListElement { text: "Hibernate" }
                        ListElement { text: "Hybrid sleep" }
                        ListElement { text: "Shut down" }
                        ListElement { text: "Lock screen" }
                    }
                }
                TimeDelaySpinBox {
                    visible: onAcPowerSuspendAutomaticallySelector.currentIndex != 0
                    type: "minute"
                    value: 20
                }
            }
            onBatteryWidget: Layouts.RowLayout {
                QQC2.ComboBox {
                    id: onBatterySuspendAutomaticallySelector
                    currentIndex: 1
                    model: ListModel {
                        ListElement { text: "No action" }
                        ListElement { text: "Sleep" }
                        ListElement { text: "Hibernate" }
                        ListElement { text: "Hybrid sleep" }
                        ListElement { text: "Shut down" }
                        ListElement { text: "Lock screen" }
                    }
                }
                TimeDelaySpinBox {
                    visible: onBatterySuspendAutomaticallySelector.currentIndex != 0
                    type: "minute"
                    value: 15
                }
            }
            onLowBatteryWidget: Layouts.RowLayout {
                enabled: false
                QQC2.ComboBox {
                    id: onLowBatterySuspendAutomaticallySelector
                    currentIndex: 1
                    model: ListModel {
                        ListElement { text: "No action" }
                        ListElement { text: "Sleep" }
                        ListElement { text: "Hibernate" }
                        ListElement { text: "Hybrid sleep" }
                        ListElement { text: "Shut down" }
                        ListElement { text: "Lock screen" }
                    }
                }
                TimeDelaySpinBox {
                    visible: onLowBatterySuspendAutomaticallySelector.currentIndex != 0
                    type: "minute"
                    value: 5
                }
            }
            fallbackWidget: Layouts.RowLayout {
                enabled: false
                QQC2.ComboBox {
                    id: fallbackSuspendAutomaticallySelector
                    currentIndex: 0
                    model: ListModel {
                        ListElement { text: "No action" }
                        ListElement { text: "Sleep" }
                        ListElement { text: "Hibernate" }
                        ListElement { text: "Hybrid sleep" }
                        ListElement { text: "Shut down" }
                        ListElement { text: "Lock screen" }
                    }
                }
                TimeDelaySpinBox {
                    visible: fallbackSuspendAutomaticallySelector.currentIndex != 0
                    type: "minute"
                    value: 20
                }
            }
        }

        CollapsedSettingsField {
            Kirigami.FormData.label: "When power button pressed:"
            expandedBuddy: expandedSuspendOnPowerButtonPressed
        }
        ExpandedSettingsField {
            id: expandedSuspendOnPowerButtonPressed
            currentEnergyProfile: energyProfileShown.currentValue

            onAcPowerWidget: QQC2.ComboBox {
                enabled: false
                currentIndex: 3
                model: ListModel {
                    ListElement { text: "Sleep" }
                    ListElement { text: "Hibernate" }
                    ListElement { text: "Hybrid sleep" }
                    ListElement { text: "Shut down" }
                    ListElement { text: "Lock screen" }
                    ListElement { text: "Turn off screen" }
                    ListElement { text: "Prompt log out dialog" }
                }
            }
            onBatteryWidget: QQC2.ComboBox {
                enabled: false
                currentIndex: 3
                model: ListModel {
                    ListElement { text: "Sleep" }
                    ListElement { text: "Hibernate" }
                    ListElement { text: "Hybrid sleep" }
                    ListElement { text: "Shut down" }
                    ListElement { text: "Lock screen" }
                    ListElement { text: "Turn off screen" }
                    ListElement { text: "Prompt log out dialog" }
                }
            }
            onLowBatteryWidget: QQC2.ComboBox {
                enabled: false
                currentIndex: 3
                model: ListModel {
                    ListElement { text: "Sleep" }
                    ListElement { text: "Hibernate" }
                    ListElement { text: "Hybrid sleep" }
                    ListElement { text: "Shut down" }
                    ListElement { text: "Lock screen" }
                    ListElement { text: "Turn off screen" }
                    ListElement { text: "Prompt log out dialog" }
                }
            }
            fallbackWidget: QQC2.ComboBox {
                currentIndex: 3
                model: ListModel {
                    ListElement { text: "Sleep" }
                    ListElement { text: "Hibernate" }
                    ListElement { text: "Hybrid sleep" }
                    ListElement { text: "Shut down" }
                    ListElement { text: "Lock screen" }
                    ListElement { text: "Turn off screen" }
                    ListElement { text: "Prompt log out dialog" }
                }
            }
        }

        CollapsedSettingsField {
            Kirigami.FormData.label: "When laptop lid closed:"
            expandedBuddy: expandedSuspendOnLaptopLidClosed
        }
        ExpandedSettingsField {
            id: expandedSuspendOnLaptopLidClosed
            currentEnergyProfile: energyProfileShown.currentValue

            onAcPowerWidget: Layouts.ColumnLayout {
                enabled: false
                QQC2.ComboBox {
                    currentIndex: 0
                    model: ListModel {
                        ListElement { text: "Sleep" }
                        ListElement { text: "Hibernate" }
                        ListElement { text: "Hybrid sleep" }
                        ListElement { text: "Shut down" }
                        ListElement { text: "Lock screen" }
                        ListElement { text: "Turn off screen" }
                    }
                }
                QQC2.CheckBox {
                    text: "Even when external monitor is connected"
                    Layouts.Layout.alignment: Qt.AlignRight
                    checked: false
                }
            }
            onBatteryWidget: Layouts.ColumnLayout {
                enabled: false
                QQC2.ComboBox {
                    currentIndex: 0
                    model: ListModel {
                        ListElement { text: "Sleep" }
                        ListElement { text: "Hibernate" }
                        ListElement { text: "Hybrid sleep" }
                        ListElement { text: "Shut down" }
                        ListElement { text: "Lock screen" }
                        ListElement { text: "Turn off screen" }
                    }
                }
                QQC2.CheckBox {
                    text: "Even when external monitor is connected"
                    Layouts.Layout.alignment: Qt.AlignRight
                    checked: false
                }
            }
            onLowBatteryWidget: Layouts.ColumnLayout {
                enabled: false
                QQC2.ComboBox {
                    currentIndex: 0
                    model: ListModel {
                        ListElement { text: "Sleep" }
                        ListElement { text: "Hibernate" }
                        ListElement { text: "Hybrid sleep" }
                        ListElement { text: "Shut down" }
                        ListElement { text: "Lock screen" }
                        ListElement { text: "Turn off screen" }
                    }
                }
                QQC2.CheckBox {
                    text: "Even when external monitor is connected"
                    Layouts.Layout.alignment: Qt.AlignRight
                    checked: false
                }
            }
            fallbackWidget: Layouts.ColumnLayout {
                QQC2.ComboBox {
                    currentIndex: 0
                    model: ListModel {
                        ListElement { text: "Sleep" }
                        ListElement { text: "Hibernate" }
                        ListElement { text: "Hybrid sleep" }
                        ListElement { text: "Shut down" }
                        ListElement { text: "Lock screen" }
                        ListElement { text: "Turn off screen" }
                    }
                }
                QQC2.CheckBox {
                    text: "Even when external monitor is connected"
                    Layouts.Layout.alignment: Qt.AlignRight
                    checked: false
                }
            }
        }

        QQC2.CheckBox {
            Kirigami.FormData.label: "While in sleep mode:"
            enabled: false
            text: "Hibernate after a period of inactivity"
            checked: false
        }

        //
        // When suspending

        QQC2.CheckBox {
            Kirigami.FormData.label: "Media playback:"
            text: "Pause media players when suspending"
            checked: true
        }
        QQC2.Button {
            text: "Configure Notifications..."
            icon.name: "notifications"
        }

        Kirigami.Separator {
            Kirigami.FormData.isSection: true
        }
        // Performance

        CollapsedSettingsField {
            id: collapsedPerformanceProfile
            Kirigami.FormData.label: "System performance:"
            expandedBuddy: expandedPerformanceProfile
        }
        ExpandedSettingsField {
            id: expandedPerformanceProfile
            currentEnergyProfile: energyProfileShown.currentValue

            TextMetrics { // TODO: part of the hack
                id: textMetrics
                text: "Balance performance and battery life" // longest string in the combobox
            }

            onAcPowerWidget: QQC2.ComboBox {
                implicitWidth: textMetrics.width + Kirigami.Units.largeSpacing * 3 // FIXME: hack
                currentIndex: 0
                model: ListModel {
                    ListElement { text: "Maximize performance" }
                    ListElement { text: "Balance performance and battery life" }
                    ListElement { text: "Maximize battery life" }
                }
            }
            onBatteryWidget: QQC2.ComboBox {
                implicitWidth: textMetrics.width + Kirigami.Units.largeSpacing * 3 // FIXME: hack
                currentIndex: 1
                model: ListModel {
                    ListElement { text: "Maximize performance" }
                    ListElement { text: "Balance performance and battery life" }
                    ListElement { text: "Maximize battery life" }
                }
            }
            onLowBatteryWidget: QQC2.ComboBox {
                implicitWidth: textMetrics.width + Kirigami.Units.largeSpacing * 3 // FIXME: hack
                currentIndex: 2
                model: ListModel {
                    ListElement { text: "Maximize performance" }
                    ListElement { text: "Balance performance and battery life" }
                    ListElement { text: "Maximize battery life" }
                }
            }
            fallbackWidget: QQC2.ComboBox {
                implicitWidth: textMetrics.width + Kirigami.Units.largeSpacing * 3 // FIXME: hack
                enabled: false
                currentIndex: 1
                model: ListModel {
                    ListElement { text: "Maximize performance" }
                    ListElement { text: "Balance performance and battery life" }
                    ListElement { text: "Maximize battery life" }
                }
            }
        }

        //
        // Wireless connections

        CollapsedSettingsField {
            id: collapsedWiFi
            Kirigami.FormData.label: "Wi-Fi:"
            expandedBuddy: expandedWiFi
        }
        ExpandedSettingsField {
            id: expandedWiFi
            currentEnergyProfile: energyProfileShown.currentValue

            onAcPowerWidget: QQC2.ComboBox {
                enabled: false
                currentIndex: 0
                model: ListModel {
                    ListElement { text: "Enabled" }
                    ListElement { text: "Disabled" }
                }
            }
            onBatteryWidget: QQC2.ComboBox {
                enabled: false
                currentIndex: 0
                model: ListModel {
                    ListElement { text: "Enabled" }
                    ListElement { text: "Disabled" }
                }
            }
            onLowBatteryWidget: QQC2.ComboBox {
                enabled: false
                currentIndex: 0
                model: ListModel {
                    ListElement { text: "Enabled" }
                    ListElement { text: "Disabled" }
                }
            }
            fallbackWidget: QQC2.ComboBox {
                currentIndex: 0
                model: ListModel {
                    ListElement { text: "Enabled" }
                    ListElement { text: "Disabled" }
                }
            }
        }

        CollapsedSettingsField {
            id: collapsedBluetooth
            Kirigami.FormData.label: "Bluetooth:"
            expandedBuddy: expandedBluetooth
        }
        ExpandedSettingsField {
            id: expandedBluetooth
            currentEnergyProfile: energyProfileShown.currentValue

            onAcPowerWidget: QQC2.ComboBox {
                enabled: false
                currentIndex: 0
                model: ListModel {
                    ListElement { text: "Enabled" }
                    ListElement { text: "Disabled" }
                }
            }
            onBatteryWidget: QQC2.ComboBox {
                enabled: false
                currentIndex: 0
                model: ListModel {
                    ListElement { text: "Enabled" }
                    ListElement { text: "Disabled" }
                }
            }
            onLowBatteryWidget: QQC2.ComboBox {
                enabled: false
                currentIndex: 0
                model: ListModel {
                    ListElement { text: "Enabled" }
                    ListElement { text: "Disabled" }
                }
            }
            fallbackWidget: QQC2.ComboBox {
                currentIndex: 0
                model: ListModel {
                    ListElement { text: "Enabled" }
                    ListElement { text: "Disabled" }
                }
            }
        }

        CollapsedSettingsField {
            id: collapsedMobileBroadband
            Kirigami.FormData.label: "Mobile broadband:"
            expandedBuddy: expandedMobileBroadband
        }
        ExpandedSettingsField {
            id: expandedMobileBroadband
            currentEnergyProfile: energyProfileShown.currentValue

            onAcPowerWidget: QQC2.ComboBox {
                enabled: false
                currentIndex: 0
                model: ListModel {
                    ListElement { text: "Enabled" }
                    ListElement { text: "Disabled" }
                }
            }
            onBatteryWidget: QQC2.ComboBox {
                enabled: false
                currentIndex: 0
                model: ListModel {
                    ListElement { text: "Enabled" }
                    ListElement { text: "Disabled" }
                }
            }
            onLowBatteryWidget: QQC2.ComboBox {
                enabled: false
                currentIndex: 0
                model: ListModel {
                    ListElement { text: "Enabled" }
                    ListElement { text: "Disabled" }
                }
            }
            fallbackWidget: QQC2.ComboBox {
                currentIndex: 0
                model: ListModel {
                    ListElement { text: "Enabled" }
                    ListElement { text: "Disabled" }
                }
            }
        }
    }
}
