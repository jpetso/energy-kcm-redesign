import QtQml 2.15
import QtQuick 2.15
import QtQuick.Controls 2.15 as QQC2
import QtQuick.Layouts 1.2 as Layouts
import org.kde.kirigami 2.14 as Kirigami

// ExpandedSettingsWidget is a collection of settings widgets for different energy profiles.
// An energy profile can be a power state (e.g. "power.ac", displaying onAcPowerWidget),
// "activity.*" (TODO: need to add dynamically loaded widgets), or other future conditions.
//
// ExpandedSettingsWidget can stand on its own, but can also lend its settings widgets
// to CollapsedSettingsField (which is responsible for moving them back and forth).
//
Layouts.ColumnLayout {
    id: root

    required property string currentEnergyProfile
    required property Item fallbackWidget
    property Item onAcPowerWidget: fallbackWidget
    property Item onBatteryWidget: fallbackWidget
    property Item onLowBatteryWidget: fallbackWidget

    readonly property bool somePowerStatesSelected: (
        onAcPowerAndBatterySelected || onLowBatterySelectedToggle.checked)
    readonly property bool someActivitiesSelected: false // FIXME: add support for activities

    readonly property bool showingPowerStates: (
        somePowerStatesSelected || currentEnergyProfile.startsWith("power."))
    readonly property bool showingActivities: (
        someActivitiesSelected || currentEnergyProfile.startsWith("activity."))

    readonly property bool singleSettingForAllEnergyProfiles: (
        !onAcPowerAndBatterySelected
        && !onLowBatterySelectedToggle.checked
        // && (no activities selected) // any more?
    )
    readonly property bool fallbackRequired: (
        !onAcPowerAndBatterySelected // full coverage: low battery can fall back to regular
        // && (not full set of activities selected) // any more?
    )

    // functions to help CollapsedSettingsField (this widget's buddy) access this widget's data
    function settingsWidget(energyProfile) {
        console.log("widget asking for: " + energyProfile);
        return (energyProfile == "power.ac" ? onAcPowerWidget
            : energyProfile == "power.battery" ? onBatteryWidget
            : energyProfile == "power.battery.low" ? onLowBatteryWidget
            : fallbackWidget);
    }
    function containerForSettingsWidget(energyProfile) {
        console.log("container asking for: " + energyProfile);
        return (energyProfile == "power.ac" ? onAcPowerContainer
            : energyProfile == "power.battery" ? onBatteryContainer
            : energyProfile == "power.battery.low" ? onLowBatteryContainer
            : fallbackContainer);
    }
    function settingsEnabledText(energyProfile) {
        console.log(containerForSettingsWidget(energyProfile).settingsEnabledText);
        return containerForSettingsWidget(energyProfile).settingsEnabledText;
    }
    function settingsEnabledIsCheckable(energyProfile) {
        return containerForSettingsWidget(energyProfile) != fallbackContainer;
    }

    // linked state: enabling/disabling one will enable/disable both of them
    /*private*/ property bool onAcPowerAndBatterySelected: false

    Binding on onAcPowerAndBatterySelected {
        id: onAcPowerAndBatterySelectedBinding
        value: (onAcPowerWidget.enabled && onBatteryWidget.enabled)
    }
    function setOnAcPowerAndBatterySelected(selected) {
        // Set the mandatory two power states, AC power and battery.
        // Low and critical battery are optional, users can fine-tune if they like.
        onAcPowerAndBatterySelectedBinding.delayed = true;
        onAcPowerWidget.enabled = selected;
        onBatteryWidget.enabled = selected;
        onAcPowerAndBatterySelectedBinding.delayed = false;
    }

    Layouts.RowLayout {
        // On AC power
        id: onAcPowerRow
        visible: showingPowerStates

        EnergyProfileIcon {
            source: "battery-full-charging"
            Layouts.Layout.alignment: Qt.AlignTop
            checked: onAcPowerAndBatterySelected
            toolTipText: i18nc(
                "@icon:tooltip, with an energy-related settings field next to it",
                "On AC power")

            onClicked: { setOnAcPowerAndBatterySelected(!onAcPowerAndBatterySelected); }

            property bool settingEnabled: onAcPowerWidget.enabled
            onSettingEnabledChanged: { setOnAcPowerAndBatterySelected(settingEnabled); }
        }

        Item {
            id: onAcPowerContainer
            implicitHeight: childrenRect.height
            implicitWidth: childrenRect.width
            children: [ onAcPowerWidget ]

            // for settingsEnabledText()
            readonly property string settingsEnabledText: i18nc(
                "@checkbox:text", "Use different settings for AC power and battery")
        }
    }

    Layouts.RowLayout {
        // On battery
        visible: showingPowerStates

        EnergyProfileIcon {
            source: "battery-full"
            Layouts.Layout.alignment: Qt.AlignTop
            checked: onAcPowerAndBatterySelected
            toolTipText: i18nc(
                "@icon:tooltip, with an energy-related settings field next to it",
                "On battery")

            onClicked: { setOnAcPowerAndBatterySelected(!onAcPowerAndBatterySelected); }

            property bool settingEnabled: onBatteryWidget.enabled
            onSettingEnabledChanged: { setOnAcPowerAndBatterySelected(settingEnabled); }
        }

        Item {
            id: onBatteryContainer
            implicitHeight: childrenRect.height
            implicitWidth: childrenRect.width
            children: [ onBatteryWidget ]

            // for settingsEnabledText()
            readonly property string settingsEnabledText: i18nc(
                "@checkbox:text", "Use different settings for AC power and battery")
        }
    }

    Layouts.RowLayout {
        // On low battery
        visible: showingPowerStates

        EnergyProfileIcon {
            id: onLowBatterySelectedToggle
            source: "battery-low"
            Layouts.Layout.alignment: Qt.AlignTop
            checked: onLowBatteryWidget.enabled
            onClicked: { onLowBatteryWidget.enabled = !onLowBatteryWidget.enabled; }
            toolTipText: i18nc(
                "@icon:tooltip, with an energy-related settings field next to it",
                "On low battery")
        }

        Item {
            id: onLowBatteryContainer
            implicitHeight: childrenRect.height
            implicitWidth: childrenRect.width
            children: [ onLowBatteryWidget ]

            // for settingsEnabledText()
            readonly property string settingsEnabledText: i18nc(
                "@checkbox:text", "Use different settings for low battery")
        }
    }

    Layouts.RowLayout {
        // Fallback, if no other energy profile applies
        visible: fallbackRequired

        EnergyProfileIcon {
            source: "applications-other"
            Layouts.Layout.alignment: Qt.AlignTop
            checked: fallbackRequired
            onCheckedChanged: { fallbackWidget.enabled = checked; }
            toolTipText: (singleSettingForAllEnergyProfiles
                ? i18nc(
                    "@icon:tooltip, with an energy-related settings field next to it",
                    "One setting for all cases")
                : i18nc(
                    "@icon:tooltip, with an energy-related settings field next to it",
                    "In all other cases"))
        }

        Item {
            id: fallbackContainer
            implicitHeight: childrenRect.height
            implicitWidth: childrenRect.width
            children: [ fallbackWidget ]

            // for settingsEnabledText()
            readonly property string settingsEnabledText: (singleSettingForAllEnergyProfiles
                ? i18nc("@checkbox:text", "One setting for all cases")
                : i18nc("@checkbox:text", "Setting used when no other condition applies"))
        }
    }
}
