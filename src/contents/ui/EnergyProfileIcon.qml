import QtQml 2.15
import QtQuick 2.15
import QtQuick.Controls 2.15 as QQC2
import org.kde.kirigami 2.14 as Kirigami

QQC2.ToolButton {
    id: root

    required property string source
    required property string toolTipText

    display: QQC2.AbstractButton.IconOnly
    icon.name: root.source

    checkable: true

    hoverEnabled: true
    QQC2.ToolTip.text: root.toolTipText
    QQC2.ToolTip.visible: hovered
    QQC2.ToolTip.delay: 0

    // steals mouse events from the ToolButton, which we only want for its presentation
    MouseArea {
        anchors.fill: parent
        onClicked: root.onClicked(mouse)
        onPressed: {}
        onPressAndHold: {}
        onDoubleClicked: {}
    }
}
