import QtQml 2.15
import QtQuick 2.15
import QtQuick.Controls 2.15 as QQC2
import QtQuick.Layouts 1.2 as Layouts
import org.kde.kirigami 2.14 as Kirigami

Layouts.RowLayout {
    required property Item expandedBuddy
    property bool expanded: false

    /*private*/ property bool didReparentSettingsWidget: false
    /*private*/ property bool requiresWidgetSwap: false
    /*private*/ property string reparentedEnergyProfile: ""
    /*private*/ property string currentEnergyProfile: (
        expandedBuddy ? expandedBuddy.currentEnergyProfile : "")

    onCurrentEnergyProfileChanged: {
        // FIXME: Something still isn't quite right with state transitions.
        //        Terminal output looks okay, but somehow the swapping of widgets
        //        in collapsed state still doesn't show up correctly in the GUI.
        //        Perhaps the ParentAnimation just neer starts/finishes because we reset
        //        the boolean again immediately? Or possibly something more banal, who knows.
        if (state == "collapsed" && didReparentSettingsWidget) {
            requiresWidgetSwap = true; // switch to "collapsed_returned" state
            requiresWidgetSwap = false; // back to "collapsed", so we can do it again if needed
        }
    }

    onExpandedBuddyChanged: { expandedBuddy.visible = expanded; }
    onExpandedChanged: { expandedBuddy.visible = expanded; }

    QQC2.ToolButton {
        id: expander
        display: QQC2.AbstractButton.IconOnly
        icon.name: expanded ? "collapse" : "expand"
        Layouts.Layout.alignment: Qt.AlignTop
        text: (expanded
            ? i18nc("@button:tooltip", "Collapse")
            : i18nc("@button:tooltip", "Show and edit this setting in different conditions")
        )
        onClicked: { expanded = !expanded; }

        hoverEnabled: true
        QQC2.ToolTip.text: text
        QQC2.ToolTip.visible: hovered
        QQC2.ToolTip.delay: 500
    }

    Item {
        id: collapsedWidgetContainer
        implicitHeight: childrenRect.height
        implicitWidth: childrenRect.width
    }

    QQC2.CheckBox {
        id: currentEnergyProfileSelectedToggle
        visible: expanded
        text: enabled ? expandedBuddy.settingsEnabledText(currentEnergyProfile) : ""
        enabled: (expandedBuddy && currentEnergyProfile
            && expandedBuddy.settingsEnabledIsCheckable(currentEnergyProfile)
        )
        checked: enabled ? expandedBuddy.settingsWidget(currentEnergyProfile).enabled : false
        onCheckedChanged: {
            if (expandedBuddy) {
                expandedBuddy.settingsWidget(currentEnergyProfile).enabled = checked;
            }
        }
    }

    state: (!currentEnergyProfile ? "initial"
        : expanded ? "expanded"
        : requiresWidgetSwap ? "collapsed_returned"
        : "collapsed"
    )
    states: [
        State {
            name: "initial"
        },
        State {
            name: "collapsed"
            StateChangeScript {
                script: {
                    didReparentSettingsWidget = true;
                    reparentedEnergyProfile = currentEnergyProfile;
                    console.log("collapsing, grab widget for " + currentEnergyProfile)
                }
            }
            ParentChange {
                target: expandedBuddy.settingsWidget(currentEnergyProfile)
                parent: collapsedWidgetContainer
                x: 0; y: 0
            }
        },
        State {
            name: "expanded"
            StateChangeScript {
                script: {
                    didReparentSettingsWidget = false;
                    requiresWidgetSwap = false;
                    console.log("expanding, return widget for " + currentEnergyProfile)
                }
            }
            ParentChange {
                target: expandedBuddy.settingsWidget(currentEnergyProfile)
                parent: expandedBuddy.containerForSettingsWidget(currentEnergyProfile)
                x: 0; y: 0
            }
        },
        State {
            name: "collapsed_returned" // will lead directly into "collapsed" => "Binding loop detected"
            StateChangeScript {
                script: {
                    didReparentSettingsWidget = false;
                    console.log("collapsed, return widget for " + reparentedEnergyProfile
                        + " (new profile: " + currentEnergyProfile + ")")
                }
            }
            ParentChange {
                target: expandedBuddy.settingsWidget(reparentedEnergyProfile)
                parent: expandedBuddy.containerForSettingsWidget(reparentedEnergyProfile)
                x: 0; y: 0
            }
        }
    ]
}
