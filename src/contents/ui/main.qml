import QtQuick 2.15
import org.kde.kirigami 2.14 as Kirigami

// Base element, provides basic features needed for all kirigami applications
Kirigami.ApplicationWindow {
    id: root

    pageStack.initialPage: Qt.resolvedUrl("EnergySavingsPage.qml")

    title: i18nc("@title:window", "Energy Saving (draft)")
}
